using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var instance = new TechTest.Class1();
            var result = instance.SomeFunction(1, 2);
            Assert.AreEqual(result, 3);
        }
    }
}
