﻿using System;

namespace TechTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var instance = new Class1();
            var result = instance.SomeFunction(1, 2);
            Console.WriteLine($"This is a test! Result: {result}");
        }
    }
}
